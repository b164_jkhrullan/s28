// //Synchronous

// console.log("Hello World");

// console.log("Goodbye");

// for(let i = 0; i <= 100; i++){
// 	console.log(i);
// }

// console.log("Hello World")

// //Asynchronous

// function printMe(){
// 	console.log("print me");
// }

// function test(){
// 	console.log("test");
// }

// setTimeout(printMe, 5000);
// test();

// fetch(url,{options}).then(response => response.json())


console.log(fetch('https://jsonplaceholder.typicode.com/posts'))






//Retrieves all posts (GET)

fetch('https://jsonplaceholder.typicode.com/posts', {
	method: 'GET'
})
.then(response => response.json())
.then(data => {
	console.log(data)
})


async function fetchData(){
	let result = await fetch('https://jsonplaceholder.typicode.com/posts');
	console.log(result);

	console.log(typeof result);
	console.log(result.body);

	let json = await result.json();

	console.log(json);


}
fetchData()



















